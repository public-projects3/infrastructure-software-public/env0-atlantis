# Overview

This repo shows the workflow in both Atlantis and Env0. It highlights the benefits of moving to Env0.

## Atlantis Setup

Let's get Atlantis setup locally on our computer.

This is a [good guide to follow.](https://www.runatlantis.io/guide/testing-locally.html)
